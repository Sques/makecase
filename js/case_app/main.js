(function() {

    var caseApp = {

        // vars - объект хранения все важных переменных приложения
        // @todo - инициализация переменных jquery через vars.jq.main и вывод ошибки в случаи отсутствия хотя бы одной переменной
        vars: {
            obj : {
                selectOptions : {},
                cImages : {},
                cropper : null,
                cropperData : {},
                canvasImageStatus : {
                    'default' : 'default',
                    'custom' : 'custom',
                    'cropped' : 'cropped'
                }
            },
            str : {
                backBtn : '.backBtnJS',
                forwardBtn : '.forwardBtnJS',
                emulatorBtn : '.emulatorBtnJS',
                emulatorHoverImg : '.emulationHoverImgJS',
                uploadImage : '.uploadImageJS',
                addPhotosBtn : '.addPhotosJS',
                draggableItem : '.draggableItemJS',
				cWidth: '.canvasWidth',
				cContainerTransform: '.canvasContainerTransform',
            },
            jq : {
                main : $('#caseAppJS'),
                form : $('#caseFormJS'),
                menu : $('#caseMenuJS'),
                topMenu : $('#siteTopMenu'),
                backBtn : $('.backBtnJS'),
                forwardBtn : $('.forwardBtnJS'),
                photosOwl : $('#casePhotosOwl'),
                filterOwl : $('#caseFilterOwl'),
                textOwl : $('#caseTextOwl'),
                backgroundOwl : $('#caseBackgroundOwl'),
                uploadedImages : $('#caseSocialUpload'),
                modalGoBackConfirm : $('#goBackConfirm'),
				setOrientationConfirm : $('#setOrientationConfirm'),
				caseTextActive : $('.caseTextActiveJS'),
				sliderText : $('#slider-text'),
				sliderTextMobile : $('#slider-text-mobile'),
				hiddenText : $('#hiddenText'),
                cPhotoContainer : $('.canvasPhotoContainerJS'),
                cTextContainer : $('.canvasTextContainerJS'),
                cFilterContainer : $('.canvasFilterContainerJS'),
                messJS : $('.messJS')
            },
            textStep : {
				size: 36,
				weight: "normal",
				family: "Playfair Display SC",
				textAlign: "left",
				text: 'Текст',
				paddingTop: 30,
				paddingBottom: 60,
				offsetY: 70,
				color: "#FFFFFF",
				background: {
					type: 'color',
					value: '#000000'
				},
				image: "",
				vertical: "true",
				reverse: 1,
				MAX_LINES: 4
            },
			tempOrientation : null,
            textStepDefault : null,
            globalCtxScale : 1,
			isTextInit : false,
			isPhotoInit : false,
			setPhotoDefault : true
        },

        model: {

            getActiveStep : function () {
                return caseApp.vars.jq.form.find('.tab-pane.active');
            },

            getActiveMenuItem : function () {
                return caseApp.vars.jq.menu.find('li.active');
            },

            getActiveStepInfo : function () {
                return {
                    index : parseInt(this.getActiveStep().index()),
                    id : this.getActiveStep().attr('id')
                };
            },

            isNextStepID : function (stepID) {

                var nextID = this.getActiveMenuItem().nextAll(':not(.hidden)').first().find('a').attr('aria-controls');
                return (nextID == stepID);

            },

            getLastBlockIndex : function () {
                return parseInt(caseApp.vars.jq.menu.find('li:not(.hidden)').length);
            },

            isValidStep : function () {

                switch (caseApp.model.getActiveStepInfo()['id']){

                    case 'caseType':
                    case 'caseMark':
                    case 'caseModel':
                    case 'casePrint':
                    case 'caseCollage':
                        var
                            a = this.getActiveStep().find('input[data-emulator="value"]').val(),
                            b = caseApp.vars.obj.selectOptions[this.getActiveStepInfo()['id']];

                        return !!a && !!b && a == b;
                        break;
                    case 'casePhoto':

                        var $file = this.getActiveStep().find('input[type="file"]');
                        if ($file[0].files.length > 0)
                        {
                            caseApp.vars.obj.selectOptions[$file.first().attr('name')] = $file[0].files;
                            return true;
                        }
                        else if (this.isNotEmptySocial())
                        {
                            return true;
                        }
                        return this.isNotEmptyOwl(caseApp.vars.jq.photosOwl);
                        break;
                    case 'caseSocialUpload':
                        return this.isNotEmptyOwl(caseApp.vars.jq.photosOwl);
                        break;
					case 'caseText':
						return !!$('.caseTextActiveJS').val();
						break;
					case 'casePhotoRasterize':
						return true; //@todo изменить
						break;
					case 'caseFilter':
						return true;
                    default:
                        return false;
                        break;
                }

            },

            setStepValue : function ($this) {

                var
                    btnValue = $this.attr('data-value'),
                    stepInfo = this.getActiveStepInfo();

                if (!!btnValue)
                {
                    this.getActiveStep().find('input[data-emulator="value"]').val(btnValue);
                    caseApp.vars.obj.selectOptions[stepInfo.id] = btnValue;
                } else
                    console.log('caseApp.model.setStepValue error');

            },

            setActiveStep : function (qualifier) {

                var $activeStep = this.getActiveMenuItem();

                if (qualifier == 'prev')
                {
                    var pa = $activeStep.prevAll(':not(.hidden)');

                    if (pa.length > 0)
                    {
                        pa.first()
                            .find('[data-toggle="tab"]')
                            .click();
                    }

                }
                else if (qualifier == 'next')
                {
                    var na = $activeStep.nextAll(':not(.hidden)');

                    if (na.length > 0)
                    {
                        na.first()
                            .removeClass('disabled')
                            .find('[data-toggle="tab"]')
                            .click();
                    }

                }
                else
                {
                    var $stepToGo = caseApp.vars.jq.menu.find('[aria-controls="'+qualifier+'"]');
                    if (!!$stepToGo.html())
                        $stepToGo.click();
                }

            },

            owlGetLength : function($owl) {

                var
                    owlLength = $owl.find('.owl-item').length

                return {
                    trueLength : owlLength,
                    withoutFirst : ( parseInt(owlLength) - 1 )
                };
            },

            isNotEmptySocial : function(){
                return !!caseApp.vars.jq.uploadedImages.find('.uploadedImagesBlockJS').html();
            },

            isNotEmptyOwl : function($owl){
                return (caseApp.model.owlGetLength($owl)['withoutFirst'] > 0);
            },

			isTextActive : function(){
				return !!caseApp.vars.jq.caseTextActive.val();
			},

			setImageForCanvas : function(objCase, canvasStatus){

				var
					img = null,
					objLength = Object.keys(objCase).length,
					canvasImages = {},
					step = 0;

				for (key in objCase)
				{

					if (!objCase[key] || !key)
						return;

					caseApp.vars.obj.cImages[key] = {};

					img = new Image();
					img.addEventListener('load', function() {
						step++;
						if (objLength == step)
						{
							setTimeout(function() {
								caseApp.model.setCanvas(canvasStatus);
								if (caseApp.vars.obj.selectOptions.caseType == 'text')
								{
                                    caseApp.model.resizeTextCanvas();
								}
								else if (caseApp.vars.obj.selectOptions.caseType == 'photo')
								{
                                    caseApp.model.resizePhotoCanvas('cTotal', caseApp.vars.jq.cPhotoContainer);
								}
								else
									console.log('Error: To resize block with canvas you need to call setImageForCanvas after select the app way. caseApp.vars.obj.selectOptions.caseType is empty');
							}, 500);

                        }

					});
					img.src = objCase[key].src;
					img.alt = objCase[key].name;
					caseApp.vars.obj.cImages[key] = img;
				}

			},

			setCanvas : function(canvasStatus){

                var arImages = caseApp.vars.obj.cImages;

                canvasStatus = canvasStatus || '';

				if (!!arImages.length > 0)
					return;

				var
					cTotal,
                    imgPosG = 0, imgPosV = 0;

				/* cTotal */
				if (caseApp.vars.obj.selectOptions.caseType == 'photo')
				{
					cTotal = document.getElementById('cTotal').getContext('2d');

					var
                        cArea = document.getElementById('cArea').getContext('2d'),
                        areaImagePosG = (arImages.cArea.height - arImages.cMask.height) / 2,
                        areaImagePosV = (arImages.cArea.width - arImages.cMask.width) / 2;

                    if (areaImagePosG < 0)
                        areaImagePosG = 0;

                    if (areaImagePosV < 0)
                        areaImagePosV = 0;

                    cTotal.canvas.height = arImages.cMask.height;
                    cTotal.canvas.width = arImages.cMask.width;

					/* cArea */
                    if (canvasStatus != 'cropped')
                    {
                        cArea.canvas.height = arImages.cArea.height;
                        cArea.canvas.width = arImages.cArea.width;

                        cArea.drawImage(arImages.cArea, 0, 0);
                    }
					/* cArea */

                    //cTotal.drawImage(arImages.cArea, areaImagePosV, areaImagePosG, cTotal.canvas.width, cTotal.canvas.height, 0, 0, cTotal.canvas.width, cTotal.canvas.height);
                    cTotal.drawImage(arImages.cArea, imgPosG, imgPosV, cTotal.canvas.width, cTotal.canvas.height);
                }
				else if (caseApp.vars.obj.selectOptions.caseType == 'text')
                {
                    var
						canvas = document.getElementById('cText'),
                        devicePixelRatio = window.devicePixelRatio || 1;

                    cTotal = canvas.getContext('2d');

                    cTotal.canvas.height = arImages.cMask.height;
                    cTotal.canvas.width = arImages.cMask.width;

					if (caseApp.vars.textStep.background.type == 'color')
					{
						cTotal.fillStyle = caseApp.vars.textStep.background.value;
						cTotal.fillRect(0, 0, cTotal.canvas.width, cTotal.canvas.height);
					}
					else
                    {
						cTotal.drawImage(caseApp.vars.textStep.background.value, 0, 0);
                    }


					cTotal.textAlign = caseApp.vars.textStep.textAlign;

					cTotal.font = parseInt(parseInt(caseApp.vars.textStep.size * devicePixelRatio) / 96 * 72) + "pt " + caseApp.vars.textStep.family;

					if (!caseApp.vars.textStep.image)
					{
						cTotal.fillStyle = caseApp.vars.textStep.color;
					}
					else
                    {
						var img = new Image();
						img.src = caseApp.vars.textStep.image;

						cTotal.fillStyle = cTotal.createPattern(img, 'repeat');
					}

					cTotal.textBaseline = "top";


					if (caseApp.vars.textStep.vertical == "true")
					{

						caseApp.vars.textStep.text = caseApp.vars.textStep.text.toUpperCase();

						var padding = 0;
						var maxWidth = canvas.height - caseApp.vars.textStep.paddingTop - caseApp.vars.textStep.paddingBottom;

						cTotal.textBaseline = "middle";

						cTotal.save();

						cTotal.translate(canvas.width / 2, canvas.height / 2);

						cTotal.font = parseInt(parseInt(canvas.width - caseApp.vars.textStep.offsetY) / 96 * 72) + "pt " + caseApp.vars.textStep.family;

						console.log(cTotal.font);

						if (caseApp.vars.textStep.reverse)
						{
							cTotal.rotate(this.inRad(270));
							padding = caseApp.vars.textStep.paddingTop;
						}
						else
						{
							cTotal.rotate(this.inRad(90));
							padding = caseApp.vars.textStep.paddingBottom;
						}

						caseApp.vars.textStep.top = -(canvas.height / 2 - padding);

						if (cTotal.measureText(caseApp.vars.textStep.text).width < maxWidth) {
							caseApp.vars.textStep.top = -(cTotal.measureText(caseApp.vars.textStep.text).width / 2);
						}

						cTotal.fillText(caseApp.vars.textStep.text, caseApp.vars.textStep.top, 0, maxWidth);

						cTotal.restore();

					}
					else
                    {

						var lineheight = devicePixelRatio * caseApp.vars.textStep.size;

						console.log(devicePixelRatio);
						cTotal.textAlign = 'center';
						cTotal.textBaseline = 'middle';

						//cTotal.font = caseApp.vars.textStep.weight + " " + parseInt(parseInt(canvas.width -
						// caseApp.vars.textStep.offsetY) / 96 * 72) + "pt " + caseApp.vars.textStep.family;


						var maxTextWidth = canvas.width - caseApp.vars.textStep.size / devicePixelRatio;

						//bicycle
						if (caseApp.vars.textStep.size > 100)
						{
							maxTextWidth = canvas.width - 50;
							if (devicePixelRatio > 1) maxTextWidth = canvas.width + 50;
						}


						var lines = this.getLines(cTotal, caseApp.vars.textStep.text, maxTextWidth);

						var linesMax = lines.slice(0, caseApp.vars.textStep.MAX_LINES);

						linesMax.map( function(line, i) {
							cTotal.fillText(
								line,
								canvas.width / 2,
								canvas.height / 2 + lineheight * ((i + 1) - (linesMax.length + 1) / 2)
							)
						});



					}

                }

				cTotal.globalCompositeOperation = 'destination-out';

				cTotal.drawImage(arImages.cMask, imgPosG, imgPosV, cTotal.canvas.width, cTotal.canvas.height);

				cTotal.globalCompositeOperation = 'destination-over';

				cTotal.drawImage(arImages.cOverlay, imgPosG, imgPosV, cTotal.canvas.width, cTotal.canvas.height);
                /* cTotal */

			},

			getLines : function(ctx, text, maxTextWidth) {

				for (var words = text.split(" "), lines = [], line = "", n = 0; n < words.length; n++) {
					var testLine = line + words[n] + " ",
						metrics = ctx.measureText(testLine),
						testWidth = metrics.width;

						if (testWidth > maxTextWidth) {
							line && line.length && line.length && " " == line[line.length - 1] && (line = line.substr(0, line.length - 1)), line.length && lines.push(line);
							for (var newWord = words[n]; newWord.length > 0;) {
								for (var measureWord = newWord; ctx.measureText(measureWord).width > maxTextWidth;)
									measureWord = measureWord.substr(0, measureWord.length - 1);

								newWord = newWord.substr(measureWord.length);

								if (newWord.length > 0) {
									measureWord.length && " " == measureWord[measureWord.length - 1] && (measureWord = measureWord.substr(0, measureWord.length - 1));
									measureWord.length && lines.push(measureWord)
								} else
									line = measureWord + " "
							}
						} else
							line = testLine
				}

				line.length && " " == line[line.length - 1] && (line = line.substr(0, line.length - 1)), line.length && lines.push(line);

				return lines;

			},

			getLinesFullWords : function(ctx, text, maxTextWidth) {

				var words = text.split(' ');

				var lines = words.reduce(
					function (acc, word) {
						var lastLineIndex = acc.length > 0 ? acc.length - 1 : 0;
						var lastLine = acc[lastLineIndex];
						var line = [lastLine, word].join(' ').trim();
						var metrics = ctx.measureText(line);
						var lineWidth = metrics.width;

						if (lineWidth > maxTextWidth) {
							console.log(acc);
							acc[lastLineIndex + 1] = word;
							console.log(acc);
						} else {
							acc[lastLineIndex] = line;
						}

						return acc;
					},
					[]
				);

				return lines;
			},

	        inRad : function(num) {
                return num * Math.PI / 180;
            },

			resizeTextCanvas : function() {

				caseApp.vars.jq.cTextContainer.find(caseApp.vars.str.cContainerTransform).css({
					'-webkit-transform': 'scale(' + 1 + ')',
					'-moz-transform': 'scale(' + 1 + ')',
					'-ms-transform': 'scale(' + 1 + ')',
					'-o-transform': 'scale(' + 1 + ')',
					'transform-origin': '0 0'
				});

				var blockW = $(".wizard").width(),
					blockH,
					percentWidth = parseInt($(".step6__img-container").width());

				if (window.outerHeight > 768) {
					blockH = parseInt($(".wizard").height(), 10);
				} else {
					blockH = parseInt($(".step6__top .choose-block").height(), 10);
				}

				blockW = parseInt(blockW / 100 * percentWidth) - 30;

				var canvasHeight = parseInt($('#cText').css("height"), 10),//170
					canvasWidth = parseInt($("#cText").css("width"), 10);

                caseApp.vars.jq.cTextContainer.find(caseApp.vars.str.cWidth).css({'width': canvasWidth, 'height': canvasHeight});

				var k1 = blockW /canvasWidth ;
				var k2 = blockH / canvasHeight;

				var koef = (k1 < k2) ? k1 : k2;

				if (koef < 1) {
					koef = parseFloat(koef.toFixed(2));
					var width_cont_new = canvasWidth * koef;
					var height_cont_new = canvasHeight * koef;

                    caseApp.vars.jq.cTextContainer.find(caseApp.vars.str.cWidth).css({'width': width_cont_new, 'height': height_cont_new});

                    caseApp.vars.jq.cTextContainer.find(caseApp.vars.str.cContainerTransform).css({
						'-webkit-transform': 'scale(' + koef + ')',
						'-moz-transform': 'scale(' + koef + ')',
						'-ms-transform': 'scale(' + koef + ')',
						'-o-transform': 'scale(' + koef + ')'
					});
				}
			},

			setDefaultColor : function() {

				caseApp.vars.jq.textOwl.find('.fontColorJS').removeClass('active');

				if (caseApp.vars.textStep.image)
				{
					caseApp.vars.jq.textOwl
						.find('a[data-image="'+ caseApp.vars.textStep.image +'"]')
						.addClass('active');
				}
				else
				{
					caseApp.vars.jq.textOwl
						.find('a[data-color="'+ caseApp.vars.textStep.color +'"]')
						.addClass('active');
				}

			},

			setDefaultFontFamily : function() {
				$('.selectActiveJS').val(caseApp.vars.textStep.family).trigger('change');
				caseApp.model.setTextFont();
			},

			setDefaultBackground : function() {

				caseApp.vars.jq.backgroundOwl.find('.fontBackgroundJS').removeClass('active');

				if (caseApp.vars.textStep.background.type == 'color')
				{
					caseApp.vars.jq.backgroundOwl
						.find('a[data-color="'+ caseApp.vars.textStep.background.value +'"]')
						.addClass('active');
				}
				else
				{
					caseApp.vars.jq.backgroundOwl
						.find('a[data-image="'+ caseApp.vars.textStep.background.value +'"]')
						.addClass('active');
				}

			},

			setTextFont :function()  {
				caseApp.vars.jq.hiddenText.css({
					fontFamily: caseApp.vars.textStep.family,
				}).text(caseApp.vars.textStep.text);
			},

            setImageCrop : function(){

                console.log('containerData BEFORE SET CROPPER', caseApp.vars.obj.cropperData.containerData);

                var
                    cArea = document.getElementById('cArea'),
                    cTotal = document.getElementById('cTotal'),
                    cropperContainerWidth = caseApp.vars.obj.cropperData.containerData.width,
                    cropperContainerHeight = caseApp.vars.obj.cropperData.containerData.cropperH,
                    containerHeight = caseApp.vars.obj.cropperData.containerData.height,
                    options = {

                        viewMode : 1,
                        dragMode: 'move',

                        background : true,
                        //restore: false,
                        guides: false,
                        center: false,
                        highlight: false,

                        cropBoxMovable: false,
                        cropBoxResizable: false,

                        toggleDragModeOnDblclick: false,

                        minContainerWidth : cropperContainerWidth,
                        minContainerHeight : cropperContainerHeight,

                        autoCropArea: 1,

                        ready : function(e){

                            // Strict mode: set crop box data first
                            var
                                ctxTotal = cTotal.getContext('2d'),
                                cropBoxWidth = ctxTotal.canvas.width * caseApp.vars.globalCtxScale,
                                cropBoxHeight = ctxTotal.canvas.height * caseApp.vars.globalCtxScale;

                            //console.log('==================================== setImageCrop ====================================');
                            //console.log('cropperContainerWidth/Height', cropperContainerWidth, cropperContainerHeight);
                            //console.log('containerHeight', containerHeight);
                            //console.log('cropBoxWidth/Height', cropBoxWidth, cropBoxHeight);

                            var cropBoxData = {
                                width: cropBoxWidth,
                                height: cropBoxHeight,
                                top: (containerHeight - cropBoxHeight) / 2 + 60, // because of top -60 for .cropper-container
                                left: (cropperContainerWidth - cropBoxWidth) / 2
                            };
                            this.cropper.setCropBoxData(cropBoxData);
                            //console.log('cropBoxData', cropBoxData);

                            if (caseApp.vars.obj.cropperData.canvasData)
                            {
                                if (
                                    caseApp.vars.obj.cropperData.globalCtxScale &&
                                    caseApp.vars.obj.cropperData.globalCtxScale != caseApp.vars.globalCtxScale
                                ) {
                                    console.log('globalScale OLD / NEW', caseApp.vars.obj.cropperData.globalCtxScale, caseApp.vars.globalCtxScale);
                                    //caseApp.vars.obj.cropperData.canvasData.height *= caseApp.vars.globalCtxScale;
                                    //caseApp.vars.obj.cropperData.canvasData.width *= caseApp.vars.globalCtxScale;
                                    //caseApp.vars.obj.cropperData.canvasData.left /= caseApp.vars.globalCtxScale;
                                    //caseApp.vars.obj.cropperData.canvasData.top /= caseApp.vars.globalCtxScale;
                                }

                                console.log('canvasData BEFORE SET', caseApp.vars.obj.cropperData.canvasData);

                                this.cropper.setCanvasData(caseApp.vars.obj.cropperData.canvasData);

                            }

                            if (caseApp.vars.obj.cropperData.data)
                            {
								/*if (cTotalCoef) {
								 caseApp.vars.obj.cropperData.canvasData.height *= cTotalCoef;
								 caseApp.vars.obj.cropperData.canvasData.width *= cTotalCoef;
								 caseApp.vars.obj.cropperData.canvasData.left *= cTotalCoef;
								 caseApp.vars.obj.cropperData.canvasData.top *= cTotalCoef;
								 }*/

                                this.cropper.setData({'rotate' : caseApp.vars.obj.cropperData.data.rotate});
                            }

                            var
								canvasSides = ['top-left', 'top-right', 'bottom-left', 'bottom-right'],
                                _cropper = this.cropper,
                                oldOffsetPos = {};

							for (var cSide in canvasSides)
							{
								$(this.cropper.canvas).append('<div data-position="' + canvasSides[cSide] + '" class="cropper-canvas-zoom cropper-canvas-zoom-' + canvasSides[cSide] + ' cropperCanvasZoomJS"></div>');

								caseApp.controller.callDraggable({

									start : function(e, ui){

                                        oldOffsetPos = ui.offset;

                                    },

									drag : function(e, ui){

										var
											zoomC = 0.008,
											itemPos = e.target.getAttribute("data-position"),
                                            offsetPos = ui.offset;

										switch(itemPos)
										{
											case 'top-left':
                                                if (oldOffsetPos.left > offsetPos.left || oldOffsetPos.top > offsetPos.top)
                                                	_cropper.zoom(zoomC);
                                                else
                                                	_cropper.zoom(-zoomC);
												break;
											case 'top-right':
                                                if (oldOffsetPos.left < offsetPos.left || oldOffsetPos.top > offsetPos.top)
                                                {
                                                    _cropper.zoom(zoomC);
                                                }
                                                else
                                                {
                                                    _cropper.zoom(-zoomC);
                                                }
												break;
											case 'bottom-left':
                                                if (oldOffsetPos.left > offsetPos.left || oldOffsetPos.top < offsetPos.top)
                                                {
                                                    _cropper.zoom(zoomC);
                                                }
                                                else
                                                {
                                                    _cropper.zoom(-zoomC);
                                                }
												break;
											case 'bottom-right':
                                                if (oldOffsetPos.left < offsetPos.left || oldOffsetPos.top < offsetPos.top)
                                                {
                                                    _cropper.zoom(zoomC);
                                                }
                                                else
                                                {
                                                    _cropper.zoom(-zoomC);
                                                }
												break;
										}

                                        oldOffsetPos = offsetPos;

                                    }

								}, $('.cropperCanvasZoomJS'));
							}

							/*
							 if (caseApp.vars.obj.cropperData.blockHeight) {
							 var blockCoef =  caseApp.model.getActiveStep().height() / caseApp.vars.obj.cropperData.blockHeight;
							 caseApp.vars.obj.cropperData.canvasData.height *= cTotalCoef;
							 caseApp.vars.obj.cropperData.canvasData.width *= cTotalCoef;
							 caseApp.vars.obj.cropperData.canvasData.x *= cTotalCoef;
							 caseApp.vars.obj.cropperData.canvasData.y *= cTotalCoef;
							 }
							 */

                            //console.log('============================================ END ===========================================');

                        }

                    };

                //console.log(options);

                caseApp.vars.obj.cropper = new Cropper(cArea, options);
                caseApp.view.colorsVisibilityToggle(caseApp.vars.jq.messJS, 'hide');

                //console.log(caseApp.vars.obj.cropper);

            },

            saveCropperOptions : function(){

                caseApp.vars.obj.cropperData['data'] = caseApp.vars.obj.cropper.getData();
                caseApp.vars.obj.cropperData['canvasData'] = caseApp.vars.obj.cropper.getCanvasData();
                caseApp.vars.obj.cropperData['cropBoxData'] = caseApp.vars.obj.cropper.getCropBoxData();
                caseApp.vars.obj.cropperData['containerBoxData'] = caseApp.vars.obj.cropper.getContainerData();
                caseApp.vars.obj.cropperData['globalCtxScale'] = caseApp.vars.globalCtxScale;

                console.log('cropperData.canvasData AFTER SAVE', caseApp.vars.obj.cropperData.canvasData);

            },

            resizePhotoCanvas : function(canvas, $container){

            	if (!canvas && !$container)
            		return;

                var
                    topMenuH = caseApp.vars.jq.topMenu.outerHeight(),
                    appMenuH = caseApp.vars.jq.menu.outerHeight(),
                    messH = $('.messJS').outerHeight(),
                    owlH = caseApp.vars.jq.photosOwl.closest('.footer').outerHeight(),
                    windowH = $(window).height(),
                    ACTIVE_BLOCK_PADDING_SIDE = 60,
                    containerH = windowH - appMenuH - topMenuH - messH - owlH - ACTIVE_BLOCK_PADDING_SIDE * 2,
                    cTotal = document.getElementById(canvas),
                    ctxTotal = cTotal.getContext('2d'),
                    canvasH = parseFloat(ctxTotal.canvas.height),
                    $canvasWidth = $container.find(caseApp.vars.str.cWidth),
                    $cContainerTrans = $container.find(caseApp.vars.str.cContainerTransform),
                    containerW = parseFloat($container.innerWidth()),
                    canvasW = parseFloat(ctxTotal.canvas.width),
					cropperH = windowH - appMenuH - topMenuH - owlH,
					k1 = containerW / canvasW,
                	k2 = containerH / canvasH,
					kCanvasW = ( k1 > 1 ) ? 1 : ( k1 <= 0 ) ? 0 : parseFloat(k1.toFixed(2)),
					kCanvasH = ( k2 > 1 ) ? 1 : ( k2 <= 0 ) ? 0 : parseFloat(k2.toFixed(2)),
					globalScale = ( kCanvasW < kCanvasH ) ? kCanvasW : kCanvasH,
					scaledCanvasW = canvasW * globalScale,
					scaledCanvasH = canvasH * globalScale;

                caseApp.model.getActiveStep().css({'height' : containerH + 'px', 'paddingBottom' : ACTIVE_BLOCK_PADDING_SIDE + 'px'});

                $canvasWidth.css({'width': (scaledCanvasW), 'height': (scaledCanvasH)});

				$cContainerTrans.css({
					'-webkit-transform': 'scale(' + globalScale + ')',
					'-moz-transform': 'scale(' + globalScale + ')',
					'-ms-transform': 'scale(' + globalScale + ')',
					'-o-transform': 'scale(' + globalScale + ')',
                    'transform-origin': '0 0'
				});

                var position = {
                    left : (containerW - scaledCanvasW) / 2 + 'px',
                    top : (containerH - scaledCanvasH) / 2 + 'px',
                    marginLeft : -canvasW / 2 + 'px'
                };

                caseApp.vars.globalCtxScale = globalScale;

                $canvasWidth.css({
                    'top' : position.top,
                    'left' : position.left,
                    'width': canvasW,
                    'height': canvasH
                });

                caseApp.vars.obj.cropperData['containerData'] = {
                    'cropperH' : cropperH,
                    'height' : containerH,
                    'width' : containerW
                };

				/*
				 console.log('==================================== setCanvasBlockStyle ====================================');
				 console.log('position', position);
				 console.log('canvasW/H', canvasW, canvasH);
				 console.log('canvasW/H with globalScale', canvasW*globalScale, canvasH*globalScale);
				 console.log('containerW/H', containerW, containerH);
				 console.log('globalCtxScale', caseApp.vars.globalCtxScale);
				 console.log('cropperData.containerData', caseApp.vars.obj.cropperData.containerData);
				 console.log('============================================ END ===========================================');
				 */

            },

            isCropperExist : function(){
                return caseApp.vars.obj.cropper;
            },

            destroyCropper : function(){
				caseApp.view.owlVisibilityToggleColors(caseApp.vars.jq.photosOwl, 'show');
				$('#footer-image-panel')
					.css('display','none')
					.removeClass('fadeInUp')
					.animateCssImageHide('fadeOutDown');

                caseApp.vars.obj.cropper.destroy();
                caseApp.vars.obj.cropper = null;
            },

            fileAPICheck : function () {

                if (window.File && window.FileReader && window.FileList && window.Blob) {
                    return true;
                } else {
                    console.log('File API не поддерживается данным браузером');
                    return false;
                }

            }

        },

        view: {

            navigationButtonToggle : function ($btn, action) {

                if (!$btn || !action)
                {
                    console.log('Not enough parameters is passed in view.navigationButtonToggle');
                    return false;
                }

                switch (action)
                {
                    case 'hide':
                        $btn.addClass('hidden');
                        break;
                    case 'show':
                        $btn.removeClass('hidden');
                        break;
                    case 'enabled':
                        $btn.removeClass('disabled');
                        break;
                    case 'disabled':
                        $btn.addClass('disabled');
                        break;
                    default:
                        console.log('unknown action in view.navigationButtonToggle');
                }

            },

            blockVisibilityToggle : function ($block, action) {

                if (!$block || !action)
                    return;

                if (action == 'hide')
                {
                    $block.addClass('hidden');
                }
                else if (action == 'show')
                {
                    $block.removeClass('hidden');
                }
            },

            newUploadedImages : function (image) {

                var
                    imageBlock = '<div class="upload-img uploadImageJS">' +
                        '<a href="#" class="upload-img-link">' +
                            '<img src="'+image.src+'" alt="image_'+image.id+'" class="img-responsive">' +
                        '</a>' +
                    '</div>';

                caseApp.vars.jq.uploadedImages.find('.uploadedImagesBlockJS').append(imageBlock);
            },

            uploadedImagesVisibility : function (action) {
                if (action == 'show')
                {
                    caseApp.vars.jq.uploadedImages.removeClass('hidden');
                }
                else if (action == 'hide')
                {
                    caseApp.vars.jq.uploadedImages.addClass('hidden');
                }
            },

            owlVisibilityToggle : function ($carousel, action) {

                var
                    $footerOwlContainer = $carousel.closest('.footer-owl-container');

                if (action == 'show' && $footerOwlContainer.is(':hidden'))
                {
                    $footerOwlContainer.show()
                        .closest(".footer").css("visibility", "visible");
                }
                else if (action == 'hide' && $footerOwlContainer.is(':visible'))
                {
                    $footerOwlContainer.hide()
                        .closest(".footer").css("visibility", "hidden");
                }

            },

			owlVisibilityToggleColors : function ($carousel, action) {

                var
                    $footerOwlContainer = $carousel.closest('.footer-owl-container');

                if (action == 'show' && $footerOwlContainer.is(':hidden'))
                {
                    $footerOwlContainer
						.css({'display' : 'block'})
						// .removeClass('fadeInDown')
						.addClass('fadeInUp')
                        .closest(".footer").css("visibility", "visible");
                }
                else if (action == 'hide' && $footerOwlContainer.is(':visible'))
                {
					$footerOwlContainer
						.css('display','none')
						.removeClass('fadeInUp');
						if (caseApp.vars.obj.selectOptions.caseType == "text") {
							$footerOwlContainer.animateCssHide('fadeOutDown');
						} else {
							$footerOwlContainer.animateCssImageHide('fadeOutDown');
						}


					//$footerOwlContainer.closest(".footer").css("visibility", "hidden");

                }

            },

			colorsVisibilityToggle : function ($block, action) {

				if (!$block || !action)
				return;

				if (action == 'hide')
				{
					$block.addClass('hidden');
				}
				if (action == 'hidefull')
				{
					$block.addClass('hidden')
						.closest(".footer").css("visibility", "hidden");
				}
				else if (action == 'show')
				{
					$block.removeClass('hidden')
						.closest(".footer").css("visibility", "visible");
				}

			},

            uploadedImageToggle : function ($this, action){

                if (!$this || !action)
                    return;

                if (action == 'show')
                {
                    $this.addClass('active');
                }
                else if (action == 'hide')
                {
                    $this.removeClass('active');
                }

            }

        },

        controller: {

            paginationGoForward : function (isClicked) {

                isClicked = isClicked || false;

                if (caseApp.model.isValidStep())
                {

                	//prolog, before set next step
                    switch (caseApp.model.getActiveStepInfo()['id'])
                    {
						case 'caseType':
                            this.changeMenu();

                            if (caseApp.vars.obj.selectOptions.caseType == "text" && !caseApp.vars.isTextInit)
                            {
                                caseApp.controller.sliderInit(caseApp.vars.jq.sliderText, 220);
                                caseApp.controller.sliderInit(caseApp.vars.jq.sliderTextMobile, 72);

                                caseApp.controller.owlInit(caseApp.vars.jq.textOwl);
                                caseApp.controller.owlInit(caseApp.vars.jq.backgroundOwl);
                                caseApp.controller.webFontsInit(caseApp.model.setDefaultFontFamily);

                                caseApp.model.setDefaultFontFamily();
                                caseApp.model.setDefaultBackground();
                                caseApp.model.setDefaultColor();
                                caseApp.model.setTextFont();

                                if (!caseApp.vars.textStepDefault)
                                    caseApp.vars.textStepDefault = $.extend(true, {}, caseApp.vars.textStep);

                                caseApp.controller.setDefaultTextStep();

                                caseApp.vars.isTextInit = true;
                            }
                            else if (caseApp.vars.obj.selectOptions.caseType == "photo" && !caseApp.vars.isPhotoInit)
							{

                                caseApp.controller.owlInit(caseApp.vars.jq.photosOwl);
                                caseApp.controller.callDroppable();
                                caseApp.controller.owlInit(caseApp.vars.jq.filterOwl);
                                caseApp.vars.isPhotoInit = true;

							}

                            break;
                        case 'casePhoto':
                            if (caseApp.vars.obj.selectOptions.photos && caseApp.vars.obj.selectOptions.photos.length > 0)
                            {
                                caseApp.controller.handleFileSelect(caseApp.vars.obj.selectOptions.photos);
                                this.clearFileInfo();
                                caseApp.view.owlVisibilityToggle(caseApp.vars.jq.photosOwl, 'show');
                                caseApp.model.setActiveStep('caseSocialUpload');
                            }
                            if (isClicked)
                                caseApp.model.setActiveStep('caseSocialUpload');
                            break;
                        case 'casePrint':
							if (caseApp.vars.obj.selectOptions.caseType == "text") {
								caseApp.view.colorsVisibilityToggle($('.textEditJS'), 'show');
							}
							break;
                        case 'caseText':

							if (caseApp.vars.obj.selectOptions.caseType == "text") {
								caseApp.view.colorsVisibilityToggle($('.textEditJS'), 'hidefull');
								//close active panels
								$('.closeColorPanelJS').each(function() {
									if ($(this).closest('.footer-owl-container').is(':visible')){
										$(this).closest('.footer-owl-container').css('display', 'none').addClass('fadeOutDown');
									}
								});

								setTimeout(function() {
									$('.panelMobileCloseJS').each(function() {
										if ($(this).closest('.footer-panel').is(':visible')) {
											$(this).click();
										}
									});
								}, '1200');
							}
                            break;
						case 'casePhotoRasterize':

                            var
                                cFilter = document.getElementById('cFilter'),
                                ctxFilter = cFilter.getContext('2d'),
                                cTotal = document.getElementById('cTotal'),
                                ctxTotal = cTotal.getContext('2d');

                            ctxFilter.canvas.height = ctxTotal.canvas.height;
                            ctxFilter.canvas.width = ctxTotal.canvas.width;

                            ctxFilter.drawImage(cTotal, 0, 0);

                            Caman(cFilter, function(){
                                this.reloadCanvasData();
                                this.resetOriginalPixelData();
                            });

                            caseApp.view.colorsVisibilityToggle(caseApp.vars.jq.messJS, 'hide');
                            caseApp.view.owlVisibilityToggle(caseApp.vars.jq.photosOwl, 'hide');
                            caseApp.view.owlVisibilityToggle(caseApp.vars.jq.filterOwl, 'show');

                            if (caseApp.model.isCropperExist())
                                caseApp.model.destroyCropper();

							break;
						case 'caseFilter':
                            caseApp.view.owlVisibilityToggle(caseApp.vars.jq.filterOwl, 'hide');
							break;
                        default:
                            break;
                    }

                    caseApp.model.setActiveStep('next');
                    this.togglePaginationBtn('forward');
                    caseApp.view.navigationButtonToggle(caseApp.vars.jq.forwardBtn, 'disabled');

                    //epilog, after set next step

					var objCase = {};

                    switch (caseApp.model.getActiveStepInfo()['id'])
                    {
						case 'caseSocialUpload':
							if (caseApp.model.isNotEmptyOwl(caseApp.vars.jq.photosOwl))
							{
                                caseApp.view.navigationButtonToggle(caseApp.vars.jq.forwardBtn, 'enabled');
							}
							break;
						case 'caseText':

							if (caseApp.vars.obj.selectOptions.caseType == "text")
							{
								caseApp.controller.setDefaultTextStep();
								caseApp.view.colorsVisibilityToggle($('.textEditJS'), 'show');

								objCase = {
									cMask: {
										src : 'images/mask-ip-6-6S.png',
										name : 'mask'
									},
									cOverlay: {
										src : 'images/overlay-silicone-ip-6-6S.png',
										name : 'overlay'
									}
								};
								caseApp.model.setImageForCanvas(objCase);
							}

							break;
						case 'casePhotoRasterize':

                             objCase = {
                                cMask: {
                                    src : 'images/mask-ip-6-6S.png',
                                    name : 'mask'
                                },
                                cArea: {
                                    src: 'images/test3.jpg',
                                    name: 'test3'
                                },
                                cOverlay: {
                                    src : 'images/overlay-silicone-ip-6-6S.png',
                                    name : 'overlay'
                                }
                            };
                            if (objCase && caseApp.vars.setPhotoDefault)
							{
                                caseApp.model.setImageForCanvas(objCase, caseApp.vars.obj.canvasImageStatus.default);
                                caseApp.vars.setPhotoDefault = false;
                            }
                            caseApp.view.colorsVisibilityToggle(caseApp.vars.jq.messJS, 'show');

                            caseApp.view.navigationButtonToggle(caseApp.vars.jq.forwardBtn, 'enabled');

							break;
						case 'caseFilter':

                            caseApp.model.resizePhotoCanvas('cFilter', caseApp.vars.jq.cFilterContainer);

                            caseApp.view.navigationButtonToggle(caseApp.vars.jq.forwardBtn, 'enabled');

							break;
                        default:
                            break;
                    }


                }
                else
                    return false;

            },

            paginationGoBack : function () {

                var
                    bCanGo = true,
                    activeStepID = caseApp.model.getActiveStepInfo()['id'];


                //prolog, before set prev step
                switch(activeStepID)
                {
                    case 'casePhoto':
                        if (caseApp.model.isNotEmptyOwl(caseApp.vars.jq.photosOwl))
                        {
                            bCanGo = false;
                            this.toggleModalConfirm(caseApp.vars.jq.modalGoBackConfirm, 'show');
                        }
                        break;
					case 'caseText':
						if  (caseApp.model.isTextActive())
						{
							bCanGo = false;
							this.toggleModalConfirm(caseApp.vars.jq.modalGoBackConfirm, 'show');
						}
						break;
                    case 'caseSocialUpload':
                        this.clearSocialBlock();
                        break;
                    case 'casePhotoRasterize':
                        this.clearSocialBlock();
						caseApp.view.colorsVisibilityToggle(caseApp.vars.jq.messJS, 'hide');

                        if (caseApp.model.isCropperExist())
                            caseApp.model.destroyCropper();

                        caseApp.model.setActiveStep('caseSocialUpload');
                        break;
                    case 'caseFilter':

                        caseApp.view.owlVisibilityToggle(caseApp.vars.jq.filterOwl, 'hide');
                        caseApp.view.colorsVisibilityToggle(caseApp.vars.jq.messJS, 'show');
                        caseApp.view.owlVisibilityToggle(caseApp.vars.jq.photosOwl, 'show');

                        break;
					case 'caseResult':

						if (caseApp.vars.obj.selectOptions.caseType == "text") {
							caseApp.controller.setDefaultTextStep();
							caseApp.view.colorsVisibilityToggle($('.textEditJS'), 'show');

							//@todo Убрать после тестирования Canvas
							/* begin */
							var objCase = {
								cMask: {
									src : 'images/mask-ip-6-6S.png',
									name : 'mask'
								},
								cOverlay: {
									src : 'images/overlay-silicone-ip-6-6S.png',
									name : 'overlay'
								}
							};
							caseApp.model.setImageForCanvas(objCase);

							/* end */

						}
						break;
                    default:
                        break;
                }

				$('a[href="#' + activeStepID + '"]').parent('li').addClass('disabled');

                if (bCanGo)
					this.stepBack();

                //epilog, after set prev step

            },

            stepBack : function(){
                this.togglePaginationBtn('back');
                caseApp.view.navigationButtonToggle(caseApp.vars.jq.forwardBtn, 'enabled');
                caseApp.model.setActiveStep('prev');
            },

            togglePaginationBtn : function (action) {

                var
                    activeBlock = caseApp.model.getActiveStepInfo(),
                    lastBlock = caseApp.model.getLastBlockIndex(),
                    backBtn = caseApp.vars.jq.backBtn,
                    forwardBtn = caseApp.vars.jq.forwardBtn;

                if (action == 'forward')
                {
                    if (activeBlock.index == 1)
                    {
                        caseApp.view.navigationButtonToggle(backBtn, 'show');
                    }
                    else if (activeBlock.id == 'caseResult')
                    {
                        caseApp.view.navigationButtonToggle(forwardBtn, 'hide');
                    }
                    else if (activeBlock.id == 'caseText' || activeBlock.id == 'casePhoto')
                    {
                        caseApp.view.navigationButtonToggle(forwardBtn, 'show');
                    }
                }
                else if (action == 'back')
                {
                    if (activeBlock.index == 1)
                    {
                        caseApp.view.navigationButtonToggle(backBtn, 'hide');
                    }
                    else if (activeBlock.index == lastBlock)
                    {
                        caseApp.view.navigationButtonToggle(forwardBtn, 'show');
                    }
                    else if (
                        activeBlock.id == 'caseText' ||
                        activeBlock.id == 'casePhoto'
                    ){
                        caseApp.view.navigationButtonToggle(forwardBtn, 'hide');
                    }

                }

            },

            emulatorCtrl : function ($this) {

                caseApp.model.setStepValue($this);
                if (caseApp.model.isValidStep())
                {
                    caseApp.view.navigationButtonToggle(caseApp.vars.jq.forwardBtn, 'enabled');
                    this.paginationGoForward();
                }
                else
                    console.log('Step not valid');

            },

            emulatorHover : function ($this, action) {

                var
                    $img = caseApp.model.getActiveStep().find(caseApp.vars.str.emulatorHoverImg),
                    thisIconImg = $this.attr('data-img');

                if ($img.length > 0 && !!thisIconImg && action == 'enter')
                {
                    $img.removeClass('hidden').attr('src', thisIconImg);
                }
                else if (action == 'leave')
                {
                    $img.addClass('hidden');
                }

            },

            changeMenu : function () {

                var
                    caseType = caseApp.vars.obj.selectOptions.caseType;

                caseApp.vars.jq.menu
                    .find('[data-toggle="tab"]')
                    .filter(function(){
                        return !!$(this).attr('data-way')
                    })
                    .each(function (){

                        var
                            $this = $(this),
                            $menuItem = $this.closest('li'),
                            thisWay = $this.attr('data-way');

                        if (caseType == thisWay)
                        {
                            caseApp.view.blockVisibilityToggle($menuItem, 'show');
                        }
                        else
                        {
                            caseApp.view.blockVisibilityToggle($menuItem, 'hide');
                        }
                    });

            },

            handleFileSelect : function (files) {

                for (var i = 0, f; f = files[i]; i++) {

                    if (!f.type.match('image.*'))
                        continue;

                    var reader = new FileReader();

                    reader.onload = (function(theFile) {
                        return function(e) {

                            var fileObj = {
                                src : e.target.result,
                                name : theFile.name
                            };
                            caseApp.controller.addPhotoOwlItem(fileObj);

                        };
                    })(f);

                    reader.readAsDataURL(f);

                }

            },

            callDraggable : function (options, $selector) {

                var
                    options = options || {},
                    $selector = $selector || $( caseApp.vars.str.draggableItem );

                $selector.draggable(options);

            },

            callDroppable : function (options, $selector) {

                var
                    options = options || {},
                    $selector = $selector || caseApp.vars.jq.cPhotoContainer.find('#cTotal');

                $selector.droppable({
                    accept: caseApp.vars.str.draggableItem,
                    drop: function(event, ui){

                        var $image = ui.draggable;

                        caseApp.model.setImageForCanvas({
                            cArea: {
                                src : $image.attr('src'),
                                name : $image.attr('alt')
                            }
                        }, caseApp.vars.obj.canvasImageStatus.custom);
                    }
                });

            },

            owlInit : function ($carousel) {
                $carousel.owlCarousel({
                    items : 2,
                    margin: 15,
                    responsive : {
                        480 : { items : 4, nav: false  }, // from zero to 480 screen width 4 items
                        568 : { items : 6, nav: false  }, // from 480 screen widthto 768 6 items
                        640 : { items : 7, nav: false  },
                        768 : { items : 8, margin: 10 }, // from 480 screen widthto 768 6 items
                        1024 : { items : 12 } // from 768 screen width to 1024 8 items
                    },
                    nav: true,
                    dots: false,
                    autoWidth : true,
                    navText: [
                        '<a href="javascript:void(0);" class="left carousel-control"><i class="fa fa-angle-left"></i></a>',
                        '<a href="javascript:void(0);" class="right carousel-control"><i class="fa fa-angle-right"></i></a>'
                    ],
                    mouseDrag : false //@todo убрать, как разберусь с несовместимостью DragAndDrop и owlTouch
                });
            },

			sliderInit : function(sliderUi, maxValue) {


				if (sliderUi.length ) {
					sliderUi.slider({
						range: "min",
						min: 12,
						max: maxValue,
						step: 2,
						value: caseApp.vars.textStep.size,
						change: function(event, ui) {
							caseApp.vars.textStep.size = ui.value;
							caseApp.controller.onChange();
						}
					});
				}
			},

            addPhotoOwlItem : function(fileObj){

                var
                    addItem,
                    newItem = '<div class="item">' +
                        '<img class="draggableItemJS" src="'+fileObj.src+'" alt="'+fileObj.name+'" />' +
                        '</div>';

                if (caseApp.model.owlGetLength(caseApp.vars.jq.photosOwl)['withoutFirst'])
                {
                    addItem = [newItem, 1];
                }
                else
                {
                    addItem = newItem;
                }

                caseApp.vars.jq.photosOwl
                    .trigger('add.owl.carousel', addItem)
                    .trigger('refresh.owl.carousel');

                caseApp.controller.callDraggable({
                    revert: 'invalid',
                    cursor: 'pointer',
                    appendTo: 'body',
                    opacity: '0,3',
                    helper : function(){

                        var
                            $this = $(this),
                            thisWidth = $this.width(),
                            thisHeight = $this.height(),
                            $thisClone = $(this).clone();

                        $thisClone.height(thisHeight).width(thisWidth).css('z-index', 9999);
                        return $thisClone;
                    }
                });
            },

            removeOwlItem : function($owl, position){

                $owl
                    .trigger('remove.owl.carousel', position)
                    .trigger('refresh.owl.carousel');

            },

            owlAddPhotosCtrl : function(){

                var activeStepID = caseApp.model.getActiveStepInfo()['id'];

                if (
                    activeStepID == 'caseSocialUpload' ||
                    activeStepID == 'casePhotoRasterize'
                ){
                    this.paginationGoBack();
                }
            },

            uploadedImagesCtrl : function(objImages, isMore){

                if(!objImages || !objImages.length > 0)
                    return;

                isMore = isMore || false;
                if (!isMore)
                	this.clearSocialBlock();

                for (var i = 0; i < objImages.length; i++)
                {
                    caseApp.view.newUploadedImages(objImages[i]);
                }
                this.paginationGoForward();

            },

            uploadedImagesSelect : function($this){

                var
                    $img = $this.find('img'),
                    fileObj = {
                        src : $img.attr('src'),
                        name : $img.attr('alt')
                    };

                if ($this.hasClass('active'))
                {
                    caseApp.vars.jq.photosOwl.find('.owl-item').each(function(index){

                        var
                            $this = $(this),
                            thisImgSrc = $this.find('img').attr('src');

                        if (thisImgSrc == fileObj.src)
                        {
                            caseApp.vars.jq.photosOwl
                                .trigger('remove.owl.carousel', index)
                                .trigger('refresh.owl.carousel');

                            return false;
                        }

                    });
                    caseApp.view.uploadedImageToggle($this, 'hide');
                }
                else
                {
                    caseApp.view.uploadedImageToggle($this, 'show');
                    caseApp.controller.addPhotoOwlItem(fileObj);
                    caseApp.view.owlVisibilityToggle(caseApp.vars.jq.photosOwl, 'show');
                }

                if (caseApp.model.isNotEmptyOwl(caseApp.vars.jq.photosOwl))
                {
                    caseApp.view.navigationButtonToggle(caseApp.vars.jq.forwardBtn, 'enabled');
                }
                else
                {
                    caseApp.view.navigationButtonToggle(caseApp.vars.jq.forwardBtn, 'disabled');
                }

            },

            clearFileInfo : function(){

                var
                    $file = caseApp.model.getActiveStep().find('input[type="file"]').first();

                delete caseApp.vars.obj.selectOptions[$file.attr('name')];
                $file.val('');

            },

            clearSocialBlock : function(){
                var
                    $uploadedImagesWrap = caseApp.vars.jq.uploadedImages.find('.uploadedImagesBlockJS');

                if ($uploadedImagesWrap.html())
                    $uploadedImagesWrap.empty();
            },

            toggleModalConfirm : function($modal, action){

                if (action == 'show' && $modal.is(':hidden'))
                {
                    $modal.addClass('modalConfirmActionJS').modal('show');
                }
                else if (action == 'hide' && $modal.is(':visible'))
                {
                    $modal.removeClass('modalConfirmActionJS').modal('hide');
                }

            },

            confirmGoBack : function($modal){

                this.toggleModalConfirm(caseApp.vars.jq.modalGoBackConfirm, 'hide');
                if (caseApp.model.getActiveStepInfo()['id'] == 'casePhoto')
				{
					var owlLength = caseApp.model.owlGetLength(caseApp.vars.jq.photosOwl)['trueLength'];
					while(owlLength > 1)
					{
						owlLength--;
						this.removeOwlItem(caseApp.vars.jq.photosOwl, owlLength);
					}
					caseApp.view.owlVisibilityToggle(caseApp.vars.jq.photosOwl, 'hide');
				}
				else if(caseApp.model.getActiveStepInfo()['id'] == 'caseText')
				{
					caseApp.controller.setDefaultTextOption('true');

					//close active panels
					$('.closeColorPanelJS').each(function() {
						if ($(this).closest('.footer-owl-container').is(':visible')){
							$(this).closest('.footer-owl-container').css('display', 'none').addClass('fadeOutDown');
						}
					});

					setTimeout(function() {
						$('.panelMobileCloseJS').each(function() {
							if ($(this).closest('.footer-panel').is(':visible')) {
								$(this).click();
							}
						});
					}, '1200');
					caseApp.controller.onChange();


					caseApp.view.colorsVisibilityToggle($('.textEditJS'), 'hidefull');
					//caseApp.controller.onChange();

				}

				this.stepBack();
            },

			confirmChangeOrient : function(){

				this.toggleModalConfirm(caseApp.vars.jq.setOrientationConfirm, 'hide');

				caseApp.controller.setDefaultTextOption(caseApp.vars.tempOrientation);
				caseApp.controller.onChange();
				caseApp.vars.tempOrientation = null;


				//close active panels
				$('.closeColorPanelJS').each(function() {
					if ($(this).closest('.footer-owl-container').is(':visible')){
						$(this).click();
					}
				});
				setTimeout(function() {
					$('.panelMobileCloseJS').each(function() {
						if ($(this).closest('.footer-panel').is(':visible')){
							$(this).click();
						}
					});
				}, '1200');

            },

            webFontsInit : function(callback) {

				WebFont.load({
					google: {
						families: [
							'Playfair+Display+SC::cyrillic-ext,latin',
							'Bad+Script::cyrillic-ext,latin',
							'Neucha::cyrillic-ext,latin',
							'Jura::cyrillic-ext,latin',
							'Kelly+Slab::cyrillic-ext,latin',
							'Ledger::cyrillic-ext,latin',
							'Lobster::cyrillic-ext,latin',
							'Lobster+Two::cyrillic-ext,latin',
							'Marck+Script::cyrillic-ext,latin',
							'PT+Serif::cyrillic-ext,latin',
							'Roboto+Condensed::cyrillic-ext,latin',
							'Roboto+Slab::cyrillic-ext,latin',
							'Ruslan+Display::cyrillic-ext,latin',
							'Russo+One::cyrillic-ext,latin',
							'Underdog::cyrillic-ext,latin'
						]
					},
					custom: {
						families: ["GothamProBlack:n4", "BigNoodleTitling"],
						urls: ["css/fonts.css"]
					},
					fontactive: function(fontFamily, fontDescription) {
						var optionFont = '<option value="' + fontFamily + '">' + fontFamily + '</option>';
						$(".selectActiveJS").append(optionFont);
					},
					active: function() {
						// sessionStorage.fonts = true;
						if (callback) callback();
					},

					loading: function() {
						console.log("loading.");
					},
					inactive: function() {
						console.log("inactive.");
					},
					fontloading: function(fontFamily, fontDescription) {
						console.log("fontloading.", fontFamily, fontDescription);
					},
					fontinactive: function(fontFamily, fontDescription) {
						console.log("fontinactive.", fontFamily, fontDescription);
					}

				});

            },

            setDefaultTextStep : function() {

				if (caseApp.vars.textStep.vertical == 'true')
				{
					caseApp.view.blockVisibilityToggle($('#frameText'), 'hide');
					caseApp.view.blockVisibilityToggle($('.fontSizeBlockJS'), 'hide');
					$('.rotateTextJs').each(function() {
						caseApp.view.blockVisibilityToggle($(this), 'show');
					});
				}
				else
                {
					caseApp.view.blockVisibilityToggle($('#frameText'), 'show');
					caseApp.view.blockVisibilityToggle($('.fontSizeBlockJS'), 'show');
					$('.rotateTextJs').each(function() {
						caseApp.view.blockVisibilityToggle($(this), 'hide');
					});
				}

				var
					activeOrientation = $('a.verticalTextJS[data-on="' + caseApp.vars.textStep.vertical +'"]');

				caseApp.view.uploadedImageToggle($('.verticalTextJS'), 'hide');
				caseApp.view.uploadedImageToggle(activeOrientation, 'show');

			},

			onChange : function() {

				caseApp.vars.textStep.text = $('.caseTextActiveJS').val();
				caseApp.vars.textStep.family = $('.selectActiveJS').val();

				this.setDefaultTextStep();
				caseApp.model.setTextFont();

				setTimeout(function() {
					caseApp.model.setCanvas(caseApp.vars.obj.cImages);
				}, 600);

			},

			syncTextInputValue : function($this, $block) {

				$block.each(function() {

					if ($(this).is('select')) {
						$(this).val($this.val()).trigger('change');
					} else {
						$(this).not($this).val($this.val());
					}
				});

			},

			setDefaultTextOption : function(orientation) {

				caseApp.vars.textStep = $.extend(true, {}, caseApp.vars.textStepDefault);

				caseApp.vars.textStep.vertical = orientation;

				caseApp.model.setDefaultFontFamily();
				caseApp.model.setDefaultBackground();
				caseApp.model.setDefaultColor();


				caseApp.controller.sliderInit(caseApp.vars.jq.sliderText, 220);
				caseApp.controller.sliderInit(caseApp.vars.jq.sliderTextMobile, 72);

				$('.caseTextActiveJS').each(function() {
					$(this).val(caseApp.vars.textStepDefault.text);
				});

				caseApp.model.setTextFont();
			},

            setFilter : function($this){

				var
					filterName = $this.attr('data-filter-name'),
					filterSettings = $this.attr('data-filter-settings');

				$this.closest('.owl-carousel').find('.filterJS').removeClass('fActive');
				$this.addClass('fActive');

                Caman("#cFilter", function(){

                    if (filterName == 'default')
                    {
                        caseApp.controller.setDefaultFilter(this);
                        this.render();
                    }
                    else
                    {
                        if(typeof this[filterName] === 'function')
                        {
                            caseApp.controller.setDefaultFilter(this);
                            this[filterName]();
                            this.render();
                        }
                        else
                            console.log('unknown filter, sorry');

                    }

                });

            },

			setDefaultFilter : function(caman){
                caman.revert(false);
            }

        },

        // control() - Запускаем необходимые методы объекта "controller"
        control: function() {

            //@todo Закоментить после тестирования
            //this.vars.obj.selectOptions['caseType'] = 'photo';
            //this.controller.changeMenu();

            //@todo Закоментить после тестирования
            //this.model.setActiveStep('casePhoto');
			//@todo Закоментить после тестирования

            return this.model.fileAPICheck();
        },

        // event() - Здесь мы регистрируем, вызываем "Обработчики событий"
        event: function() {

            caseApp.vars.jq.main
                .on('click', caseApp.vars.str.backBtn, function(){
                    caseApp.controller.paginationGoBack();
                })
                .on('click', caseApp.vars.str.forwardBtn, function(){
                    if (!$(this).hasClass('disabled'))
                        caseApp.controller.paginationGoForward('true');
                })
                .on('click', caseApp.vars.str.emulatorBtn, function(e){
                    e.preventDefault();
                    caseApp.controller.emulatorCtrl($(this));
                })
                .on('mouseenter', caseApp.vars.str.emulatorBtn, function(e){
                    caseApp.controller.emulatorHover($(this), 'enter');
                })
                .on('mouseleave', caseApp.vars.str.emulatorBtn, function(e){
                    caseApp.controller.emulatorHover($(this), 'leave');
                })
                .on('change', 'input[type="file"]', function(){
                    caseApp.controller.paginationGoForward();
                })
                .on('click', caseApp.vars.str.uploadImage, function(e){
                    e.preventDefault();
                    caseApp.controller.uploadedImagesSelect($(this));
                })
                .on('dblclick', '#cTotal', function(){
					if (caseApp.vars.obj.selectOptions.caseType == "photo") {
						caseApp.view.owlVisibilityToggleColors(caseApp.vars.jq.photosOwl, 'hide');
						$('#footer-image-panel').css('display', 'block').addClass('fadeInUp');
						caseApp.model.setImageCrop();
					}
                });

            caseApp.vars.jq.photosOwl
                .on('click', caseApp.vars.str.addPhotosBtn, function(e){
                    e.preventDefault();
                    caseApp.controller.owlAddPhotosCtrl();
                })
                .on('dblclick', '.item', function(e){
                    e.preventDefault();

                    var $image = $(this).find('img');

                    caseApp.model.setImageForCanvas({
                        cArea: {
                            src : $image.attr('src'),
                            name : $image.attr('alt')
                        }
                    }, caseApp.vars.obj.canvasImageStatus.custom);
                });

            caseApp.vars.jq.filterOwl.on('click', '.filterJS:not(.fActive)', function(){
            	caseApp.controller.setFilter($(this));
            });

            caseApp.vars.jq.modalGoBackConfirm
                .on('click', '.confirmBtnYes', function(e){
                    e.preventDefault();
                    caseApp.controller.confirmGoBack();
                })
                .on('click', '.confirmBtnNo', function(e){
                    e.preventDefault();
                    caseApp.controller.toggleModalConfirm(caseApp.vars.jq.modalGoBackConfirm, 'hide');
                });

			caseApp.vars.jq.setOrientationConfirm
				.on('click', '.confirmBtnYes', function(e){
					e.preventDefault();
					caseApp.controller.confirmChangeOrient();
				})
				.on('click', '.confirmBtnNo', function(e){
					e.preventDefault();
					caseApp.controller.toggleModalConfirm(caseApp.vars.jq.setOrientationConfirm, 'hide');
				});

            //text
			$(document)
				.on('change, keyup', '.caseTextActiveJS', function(e) {

					if (!(e.keyCode > 36 && e.keyCode < 41)) {
						e.preventDefault();
						caseApp.controller.syncTextInputValue($(this), $('.caseTextActiveJS'));

						if  (caseApp.model.isTextActive()) {
							caseApp.view.navigationButtonToggle(caseApp.vars.jq.forwardBtn, 'enabled');
						} else {
							caseApp.view.navigationButtonToggle(caseApp.vars.jq.forwardBtn, 'disabled');
						}

						caseApp.controller.onChange();
					}
				})
				.on('select2:select', '.selectActiveJS', function(e){
					e.preventDefault();
					//caseApp.vars.textStep.family = $(this).val();
					caseApp.controller.syncTextInputValue($(this), $('.selectActiveJS'));
					caseApp.controller.onChange();
				})
				.on('click', '.rotateTextJs', function(e) {
					e.preventDefault();
					caseApp.vars.textStep.reverse = !caseApp.vars.textStep.reverse;
					caseApp.controller.onChange();
				})
				.on('click', '.fontColorJS', function(e) {
					e.preventDefault();

					caseApp.view.uploadedImageToggle(caseApp.vars.jq.textOwl.find('.fontColorJS'), 'hide');
					caseApp.view.uploadedImageToggle($(this), 'show');

					caseApp.vars.textStep.image = $(this).attr('data-image');
					caseApp.vars.textStep.color = $(this).attr('data-color');

					caseApp.controller.onChange();
				})
				.on('click', '.fontBackgroundJS', function(e) {
					e.preventDefault();

					caseApp.view.uploadedImageToggle(caseApp.vars.jq.backgroundOwl.find('.fontBackgroundJS'), 'hide');
					caseApp.view.uploadedImageToggle($(this), 'show');

					if ($(this).attr('data-image').length > 0) {
						var image = new Image();
						image.src = $(this).attr('data-image');

						caseApp.vars.textStep.background = {
							type: 'image',
							value: image
						};

					} else {
						caseApp.vars.textStep.background = {
							type: 'color',
							value: $(this).attr('data-color'),
						}
					}

					caseApp.controller.onChange();
				})
				.on('change', '.fontSizeJS', function(e) {
					e.preventDefault();
					caseApp.vars.textStep.size = $(this).val();
					caseApp.controller.onChange();
				})
				.on('click', '.verticalTextJS', function(e) {
					e.preventDefault();
					caseApp.controller.toggleModalConfirm(caseApp.vars.jq.setOrientationConfirm, 'show');
					caseApp.vars.tempOrientation = $(this).attr('data-on');
				})
				.on('click', '.closeColorPanelJS', function(e) {
					e.preventDefault();
					caseApp.view.owlVisibilityToggleColors($(this).closest('.owl-fill-color-container'), 'hide');
					//caseApp.view.colorsVisibilityToggle($('.textEditJS'), 'show');

				})
				.on('click', '.changeBgJs', function(e) {
					e.preventDefault();
					caseApp.view.owlVisibilityToggleColors(caseApp.vars.jq.backgroundOwl, 'show');
					caseApp.view.colorsVisibilityToggle($('.textEditJS'), 'hide');

				})
				.on('click', '.changeColorJs', function(e) {
					e.preventDefault();
					caseApp.view.owlVisibilityToggleColors(caseApp.vars.jq.textOwl, 'show');
					caseApp.view.colorsVisibilityToggle($('.textEditJS'), 'hide');
				})
				.on('click', '.panelMobileJS', function(e) {
					e.preventDefault();

					if (caseApp.vars.textStep.vertical == 'true') {
						$('#footer-hor-panel').css('display', 'block').addClass('fadeInUp');
					} else {
						$('#footer-vert-panel').css('display', 'block').addClass('fadeInUp');
					}
				})
				.on('click', '.panelZoomJS', function(e) {
					e.preventDefault();
					$('#footer-vert-slider-panel').css('display', 'block').addClass('fadeInUp');
				})
				.on('click', '.panelMobileCloseJS', function(e) {
					e.preventDefault();

					$(this).closest('.footer-panel')
						.css('display','none')
						.removeClass('fadeInUp')
						.animateCssHide('fadeOutDown');
					//addClass('fadeOutDown').css('display', 'none');

				})
				.on('click', '.clearTextJS', function(e) {
					e.preventDefault();
					$('.caseTextActiveJS').val('');
					caseApp.controller.syncTextInputValue($(this), $('.caseTextActiveJS'));
					caseApp.view.navigationButtonToggle(caseApp.vars.jq.forwardBtn, 'disabled');
					caseApp.controller.onChange();
				});


			$(window).resize(function () {

                console.log('RESIZE');

                if (caseApp.model.isCropperExist())
				{
                    caseApp.model.destroyCropper();
                    caseApp.view.colorsVisibilityToggle(caseApp.vars.jq.messJS, 'show');
				}

                if (caseApp.model.getActiveStepInfo()['id'] == 'casePhotoRasterize')
                {
                    caseApp.model.resizePhotoCanvas('cTotal', caseApp.vars.jq.cPhotoContainer);
                }

                if (caseApp.model.getActiveStepInfo()['id'] == 'caseFilter')
                {
                    caseApp.model.resizePhotoCanvas('cFilter', caseApp.vars.jq.cFilterContainer);
                }

                if (caseApp.model.getActiveStepInfo()['id'] == 'caseText')
				{
                    caseApp.model.resizeTextCanvas();
				}
            });

            $('.cropControlJS').on('click', 'a', function(e){
                e.preventDefault();

                var
                    $this = $(this),
                    thisAction = $this.attr('href').substr(1, this.length);

                switch(thisAction){
                    case 'close':
                        //save before destroy
                        if (caseApp.model.isCropperExist())
                        {
                            caseApp.model.setImageForCanvas({
                                cArea: {
                                    src : caseApp.vars.obj.cropper.getCroppedCanvas().toDataURL(),
                                    name : caseApp.vars.obj.cImages.cArea.name
                                }
                            }, caseApp.vars.obj.canvasImageStatus.cropped);
                            caseApp.model.saveCropperOptions();
                            caseApp.model.destroyCropper();
                            caseApp.view.colorsVisibilityToggle(caseApp.vars.jq.messJS, 'show');
                        }
                        break;
                    case '2degreesRotateLeft':
                        if (caseApp.model.isCropperExist())
                            caseApp.vars.obj.cropper.rotate(-2);
                        break;
                    case '2degreesRotateRight':
                        if (caseApp.model.isCropperExist())
                            caseApp.vars.obj.cropper.rotate(2);
                        break;
                    case '90degreesRotateRight':
                        if (caseApp.model.isCropperExist())
                            caseApp.vars.obj.cropper.rotate(90);
                        break;
                    case 'SetDefaultCanvas':
                        if (caseApp.model.isCropperExist())
						{
                            caseApp.model.destroyCropper();
                            caseApp.view.colorsVisibilityToggle(caseApp.vars.jq.messJS, 'show');
						}
                        break;
                    default:
                        break;
                }

            });
        },

        init: function() {
            if (this.control())
                this.event();
        }

    };

    // запускаем init() - выполняет запуск всего кода
    caseApp.init();
    window.caseApp = caseApp;

}());
