(function (){

    var socialApiApp = {

        vars : {
            fb : {
                scriptID : 'facebook-jssdk',
                scriptSrc : '//connect.facebook.net/en_US/sdk.js',
                appID : '307201843015915',
                scope : 'user_photos'
            },
            vk : {
                scriptID : 'vk_api_transport',
                scriptSrc : 'https://vk.com/js/api/openapi.js?139',
                appID : '5873112',
                scope : '4'
            },
            ig : {
                appID : '52a16b9132b446bbab7d3653c6c84973',
                redirectUri : window.location.href,
                popupWidth : 700,
                popupHeight : 500,
                accessToken : null
            },
            objImages : [],
            socialData : null,
            defaultSocialData : {
                'method' : null,
                'offset' : 0,
                'limit' : 42,
                'hasMore' : false,
                'isMore' : false
            }
        },

        controller : {

            insertAPIscript : function(id, src){

                var js, fjs = document.getElementsByTagName('script')[0];
                if (document.getElementById(id)) {return;}
                js = document.createElement('script'); js.id = id;
                js.src = src;
                js.async = true;
                fjs.parentNode.insertBefore(js, fjs);

            },

            initFB : function(){

                window.fbAsyncInit = function() {
                    FB.init({
                        appId      : socialApiApp.vars.fb.appID,
                        xfbml      : true,
                        status     : true,
                        version    : 'v2.8'
                    });
                    FB.AppEvents.logPageView();
                };

            },

            initVK : function(){

                window.vkAsyncInit = function() {
                    VK.init({
                        apiId: socialApiApp.vars.vk.appID
                    });
                };

            },

            fbCtrl : function(){

                FB.login(function(loginResponse)
                {
                    if (loginResponse.authResponse)
                    {
                        socialApiApp.controller.fbGetAllPhotos();
                    }
                    else
                        console.log('User cancelled login or did not fully authorize.');

                }, {scope: socialApiApp.vars.fb.scope});

            },

            fbGetAllPhotos : function(){

                var sData = socialApiApp.vars.socialData || null;
                if (!sData) return;

                var link, parameters;
                if (!!sData.offset)
                {
                    link = sData.offset;
                    parameters = {}
                }
                else
                {
                    link = '/me/photos/';
                    parameters = {
                        fields: 'images',
                        type : 'uploaded',
                        limit : sData.limit
                    };
                }

                FB.api(link, parameters, function(photos)
                {
                    if (photos && photos.data && photos.data.length)
                    {
                        socialApiApp.controller.clearImageObj();
                        for (var j = 0; j < photos.data.length; j++)
                        {
                            var photo = photos.data[j];
                            socialApiApp.controller.pushImageObj(photo.id, photo.images[0].source);
                        }

                        if (photos.paging.next)
                        {
                            socialApiApp.vars.socialData.hasMore = true;
                            socialApiApp.vars.socialData.offset = photos.paging.next;
                        }
                        else
                        {
                            socialApiApp.vars.socialData.hasMore = false;
                        }

                        if (socialApiApp.vars.objImages.length > 0)
                            caseApp.controller.uploadedImagesCtrl(socialApiApp.vars.objImages, sData.isMore);
                    }
                    else if (photos.error)
                    {
                        console.log(photos.error);
                    }
                });

            },

            vkCtrl : function(){

                VK.Auth.login(function(userInfo){
                    if (userInfo.status === 'connected' && userInfo.session)
                    {
                        socialApiApp.controller.vkGetAllPhotos();
                        //socialApiApp.controller.vkGetAlbums();
                    }
                }, socialApiApp.vars.vk.scope);

            },

            vkGetAllPhotos : function(){

                var sData = socialApiApp.vars.socialData || null;
                if (!sData) return;

                var parameters = {
                    photo_sizes : 1,
                    no_service_albums : 0,
                    count : sData.limit,
                    offset : sData.offset,
                    skip_hidden : 1
                };

                VK.api('photos.getAll', parameters, function(photos)
                {
                    if (photos.response && photos.response[0] > 0)
                    {
                        socialApiApp.controller.clearImageObj();
                        for (var j = 0; j < photos.response.length; j++)
                        {
                            var
                                src = false,
                                w = false,
                                y = false,
                                z = false,
                                x = false;

                            if (!!photos.response[j].sizes)
                            {

                                for (var k = 0; k < photos.response[j].sizes.length; k++)
                                {
                                    if (photos.response[j].sizes[k].type == 'w')
                                    {
                                        src = photos.response[j].sizes[k].src;
                                        w = true;
                                    }
                                    if (w == false && photos.response[j].sizes[k].type == 'z')
                                    {
                                        src = photos.response[j].sizes[k].src;
                                        z = true;
                                    }
                                    if (w == false && z == false && photos.response[j].sizes[k].type == 'y')
                                    {
                                        src = photos.response[j].sizes[k].src;
                                        y = true;
                                    }
                                    if (w == false && z == false && y == false && photos.response[j].sizes[k].type == 'x')
                                    {
                                        src = photos.response[j].sizes[k].src;
                                        x = true;
                                    }
                                }
                                socialApiApp.controller.pushImageObj(photos.response[j].pid, src);

                                if (photos.response.length == (j+1))
                                {
                                    socialApiApp.vars.socialData.offset += (j-1);

                                    var
                                        offset = socialApiApp.vars.socialData.offset,
                                        max = photos.response[0] - 1;

                                    socialApiApp.vars.socialData.hasMore = (offset < max);
                                }

                            }

                        }
                        if (socialApiApp.vars.objImages.length > 0)
                            caseApp.controller.uploadedImagesCtrl(socialApiApp.vars.objImages, sData.isMore);
                    }
                    else if (albums.error)
                        console.log(photos.error.error_msg);
                });
            },

            igCtrl : function() {

                var
                    accessToken = false,
                    popupLeft = (window.screen.width - socialApiApp.vars.ig.popupWidth) / 2,
                    popupTop = (window.screen.height - socialApiApp.vars.ig.popupHeight) / 2;

                var popup = window.open(socialApiApp.vars.ig.redirectUri, '', 'width='+socialApiApp.vars.ig.popupWidth+',height='+socialApiApp.vars.ig.popupHeight+',left='+popupLeft+',top='+popupTop+'');
                popup.addEventListener("DOMContentLoaded", function(){

                    if (window.location.hash.length == 0)
                    {
                        popup.open('https://instagram.com/oauth/authorize/?client_id='+socialApiApp.vars.ig.appID+'&redirect_uri='+socialApiApp.vars.ig.redirectUri+'&response_type=token', '_self');
                    }

                    var interval = setInterval(function() {
                        try
                        {
                            if (popup.location.hash.length)
                            {
                                clearInterval(interval);
                                socialApiApp.vars.ig.accessToken = popup.location.hash.slice(14);
                                popup.close();
                                socialApiApp.controller.igGetPhotos();
                            }
                        }
                        catch(evt)
                        {
                            console.log(evt);
                            clearInterval(interval);
                        }
                    }, 100);
                });

            },

            igGetPhotos : function() {

                var accessToken = socialApiApp.vars.ig.accessToken;
                if (!accessToken)
                    return;

                var sData = socialApiApp.vars.socialData || null;
                if (!sData) return;

                var link, parameters;
                if (!!sData.offset)
                {
                    link = sData.offset;
                    parameters = {}
                }
                else
                {
                    link = 'https://api.instagram.com/v1/users/self/media/recent/';
                    parameters = {
                        access_token: accessToken,
                        count :  sData.limit
                    };
                }

                $.ajax({
                    url: link,
                    dataType: 'jsonp',
                    type: 'GET',
                    data: parameters,
                    success: function(photos){
                        console.log(photos);
                        if (photos.data && photos.data.length > 0)
                        {
                            socialApiApp.controller.clearImageObj();
                            for (var i = 0; i < photos.data.length; i++)
                            {
                                var photo = photos.data[i];
                                socialApiApp.controller.pushImageObj(photo.id, photo.images['standard_resolution'].url);
                            }

                            if (photos.pagination.next_url)
                            {
                                socialApiApp.vars.socialData.hasMore = true;
                                socialApiApp.vars.socialData.offset = photos.pagination.next_url;
                            }
                            else
                            {
                                socialApiApp.vars.socialData.hasMore = false;
                            }

                            if (socialApiApp.vars.objImages.length > 0)
                                caseApp.controller.uploadedImagesCtrl(socialApiApp.vars.objImages, sData.isMore);
                        }
                    },
                    error: function(data){
                        console.log(data);
                    }
                });

            },

            pushImageObj : function(id, src){
                socialApiApp.vars.objImages.push({
                    id : id,
                    src : src
                });
            },

            clearImageObj : function(){
                socialApiApp.vars.objImages = [];
            },

            initSocialData : function(method){
                socialApiApp.vars.socialData = socialApiApp.vars.defaultSocialData;
                socialApiApp.vars.socialData.method = method;
            }

        },

        event : function(){

            $('body')
                .on('click', '.faceBookJS', function(e){
                    e.preventDefault();
                    socialApiApp.controller.initSocialData('fbGetAllPhotos');
                    socialApiApp.controller.fbCtrl();
                })
                .on('click', '.vkJS', function(e){
                    e.preventDefault();
                    socialApiApp.controller.initSocialData('vkGetAllPhotos');
                    socialApiApp.controller.vkCtrl();
                })
                .on('click', '.IGJS', function(e){
                    e.preventDefault();
                    socialApiApp.controller.initSocialData('igGetPhotos');
                    socialApiApp.controller.igCtrl();
                });

            $(window).scroll(function(){

                if (caseApp.model.getActiveStepInfo()['id'] == 'caseSocialUpload')
                {
                    var sData = socialApiApp.vars.socialData;
                    if ($(window).scrollTop() == $(document).height() - $(window).height() && sData.hasMore)
                    {
                        if (!sData.isMore)
                        {
                            socialApiApp.vars.socialData.isMore = true;
                        }

                        socialApiApp.controller[sData.method]();
                    }
                }

            });
        },

        control : function (){

            this.controller.insertAPIscript(this.vars.fb.scriptID, this.vars.fb.scriptSrc);
            this.controller.initFB();

            this.controller.insertAPIscript(this.vars.vk.scriptID, this.vars.vk.scriptSrc);
            this.controller.initVK();

        },

        init : function() {
            this.control();
            this.event();
        }

    };

    socialApiApp.init();
    window.socialApiApp = socialApiApp;

}());