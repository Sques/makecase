$(document).ready(function(){
	$("body").on("click", '.navbar-toggle', function () {
		$(this).toggleClass("active");
	});

	$(".owl-theme-main").owlCarousel({
		loop: true,
		autoplay:true,
		autoplayTimeout:5000,
		autoplayHoverPause:true,
		navigation : true, // Show next and prev buttons
		slideSpeed : 500,
		paginationSpeed : 400,
		//singleItem:true,
		// "singleItem:true" is a shortcut for:
		items : 1,
		itemsDesktop : false,
		itemsDesktopSmall : false,
		itemsTablet: false,
		itemsMobile : false,
		nav: true,
		navText: [
		'<a href="javascript:void(0);" class="left carousel-control"><i class="fa fa-angle-left"></i></a>',
		'<a href="javascript:void(0);" class="right carousel-control"><i class="fa fa-angle-right"></i></a>'
		]
	});

	/* ripple animation */
	var hoverOrClick = function (e) {
		//e.preventDefault();

		var $div = $('<div/>'),
		btnOffset = $(this).offset(),
		xPos = e.pageX - btnOffset.left,
		yPos = e.pageY - btnOffset.top,
		_h = 50,
		_w = 50;


		$div.addClass('ripple-effect');
		var $ripple = $(".ripple-effect");

		$ripple.css("height", $(this).height());
		$ripple.css("width", $(this).height());
		$div
		.css({
			top: yPos - (_h/2),
			left: xPos - (_w/2),
			background: $(this).data("ripple-color")
		})
		.appendTo($(this));

		window.setTimeout(function(){
			$div.remove();
		}, 2000);
	}
	$('.ripple').click(hoverOrClick);

	/*
	$('a[data-toggle="tab"]').on('show.bs.tab', function (e) {

		var $target = $(e.target);

		$target.parent('li').nextAll().addClass('disabled');
	});
	$(".next-step").click(function (e) {
		var length =  $('.wizard .nav-tabs li').length;
		var $active = $('.wizard .nav-tabs li.active');

		$active.next().removeClass('disabled');
		nextTab($active);

		if ( $active.index()+1 != length ) {
			$active.addClass('disabled');
		};
		
		if ( $active.index()+2 == length ) {
			$(".btn.btn-primary.next-step").hide();
		} else {
			$(".btn.btn-primary.next-step").show();
		};
		

		hideFooterOwl();
	});
	$(".prev-step").click(function (e) {
		var $active = $('.wizard .nav-tabs li.active');
        var length =  $('.wizard .nav-tabs li').length;
		$active.prev().removeClass('disabled');
		prevTab($active);
		hideFooterOwl();

		if ( $active.index()+2 == length ) {
			$(".btn.btn-primary.next-step").hide();
		} else {
			$(".btn.btn-primary.next-step").show();
		};
	});

	function hideFooterOwl(){
		if ( $("#step5").hasClass('active') ) {
			$(".footer-owl-container").show();
			$(".footer").css("visibility", "visible");
		} else {
			$(".footer-owl-container").hide()
			$(".footer").css("visibility", "hidden");
		}
	};
	*/

	/* select custom */
	$('.select-colors').each(function() {

		var $this = $(this),
		numberOfOptions = $(this).children('option').length;

		$this.addClass('s-hidden');
		$this.wrap('<div class="select"></div>');
		$this.after('<div class="styledSelect"></div>');

		var $styledSelect = $this.next('div.styledSelect');

		$styledSelect.text($this.children('option').eq(0).text()).attr("rel", $this.children('option').eq(0).val());

		var $list = $('<ul />', {
			'class': 'options'
		}).insertAfter($styledSelect);

		for (var i = 0; i < numberOfOptions; i++) {
			$('<li />', {
				text: $this.children('option').eq(i).text(),
				rel: $this.children('option').eq(i).val()
			}).appendTo($list);
		}

		var $listItems = $list.children('li');

		$styledSelect.click(function(e) {
			e.stopPropagation();

			var thisRel = $(this).attr("rel");
			var innerList = $(this).next('ul.options').find("li");
			innerList.show();
			
			$('div.styledSelect.active').each(function() {
				$(this).removeClass('active').next('ul.options').hide();
			});
			$(this).toggleClass('active').next('ul.options').toggle();

			innerList.each(function(){
				var $this = $(this);
				var innerRel = $this.attr("rel");

				if (thisRel == innerRel) 
					$this.hide();
			});
		});

		$listItems.click(function(e) {
			e.stopPropagation();
			$styledSelect.text($(this).text()).removeClass('active').attr("rel", $(this).attr('rel'));
			$this.val($(this).attr('rel'));
			$list.hide();
		});

		$(document).click(function() {
			$styledSelect.removeClass('active');
			$list.hide();
		});
	});

    //if (window.select2 && getType.toString.call(window.select2) === '[object Function]')
    $(".text-edit__fonts-select").select2();

    /* disable progress bar*/
    var disableBlocks = '<div class="disable-block"></div>';
    $('.wizard .nav-tabs li').append(disableBlocks);


    /* slider */
    /*if ( $('#slider-text').length ){
    	$('#slider-text').slider({
    		range: "min"
    	});
    }

    if ( $('#slider-text-mobile').length ){
    	$('#slider-text-mobile').slider({
    		range: "min"
    	});
    }
    }*/



	$.fn.extend({
		animateCssHide: function (animationName) {
			var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
			this.addClass('animated ' + animationName)
				.css({'display' : 'block'})
				.one(animationEnd, function() {
					if ($(this).hasClass('fadeInUp')) return;
					$(this).removeClass('animated ' + animationName);
					$(this).closest(".footer").css("visibility", "visible");
					$(this).css({'display' : 'none'})
					$('.textEditJS').removeClass('hidden');
				});
		}
	});

	$.fn.extend({
		animateCssImageHide: function (animationName) {
			var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
			this.addClass('animated ' + animationName)
				.css({'display' : 'block'})
				.one(animationEnd, function() {
					if ($(this).hasClass('fadeInUp')) return;
					$(this).removeClass('animated ' + animationName);
					$(this).closest(".footer").css("visibility", "visible");
					$(this).css({'display' : 'none'})
				});
		}
	});

});


// function nextTab(elem) {
// 	$(elem).next().find('a[data-toggle="tab"]').click();
// }
// function prevTab(elem) {
// 	$(elem).prev().find('a[data-toggle="tab"]').click();
// }